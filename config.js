const config = {
  environment: process.env.NODE_ENV || 'dev',
  server: {
    port: process.env.PORT || 8080
  },
  mongo: {
    url: process.env.MONGO_DB_URI || 'mongodb://localhost/cart_db',
    sessions: 'sessions'
  },
  secret: '123',
  name: 'nodeStore',
  lang: 'en-US',
  currency: 'USD'
};

module.exports = config;
