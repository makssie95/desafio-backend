const Router = require('express').Router;
const router = new Router();
const Security = require('./lib/security');

const productController = require('./controller/product');
const checkoutController = require('./controller/checkout');
const cartController = require('./controller/cart');

router.use('/checkout', checkoutController);
router.use('/cart', cartController);
router.use('/products', productController);

router.route('/').get((req, res) => {
  res.json({
    message: 'Use o nonce (token) abaixo em toda request do Cart para poder ter validação, um novo é gerado para cada sessão',
    nonce: Security.md5(req.sessionID + req.headers['user-agent'])
  });
});

module.exports = router;
