const controller = require('../lib/controller');
const Router = require('express').Router;
const router = new Router();
const Products = require('../model/product/schema');
const Security = require('../lib/security');


router.route('/').get((req, res) => {
  if (!req.session.cart) {
    req.session.cart = {
      items: [],
      totals: 0.00,
      formattedTotals: ''
    };
  }
  Products.find({
    value: {
      $gt: 0
    }
  }).sort({
    value: -1
  }).limit(6).then((products) => {
    res.json({
      products,
      nonce: Security.md5(req.sessionID + req.headers['user-agent'])
    });
  }).catch((err) => {
    res.status(400).send('Bad request');
  });
})
  .post((req, res) => {
    Products.create(req.body)
      .then(obj => res.json(obj))
      .catch(err => res.json(err));
  })
  .delete((req, res) => {
    Products.remove(req.body._id)
      .then(obj => res.json(obj))
      .catch(err => res.json(err));
  });

router.route('/:id')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findById(...args))
  .delete((...args) => controller.remove(...args));

module.exports = router;
