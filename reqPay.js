const request = {
  amount: 0,
  card_number: '4111111111111111',
  card_cvv: '123',
  card_expiration_date: '0922',
  card_holder_name: 'João das Neves',
  customer: {
    external_id: '#3311',
    name: 'João das Neves Braulio',
    type: 'individual',
    country: 'br',
    email: 'joaodasneves@got.com',
    documents: [
      {
        type: 'cpf',
        number: '00000000000'
      }
    ],
    phone_numbers: ['+5511999998888', '+5511888889999'],
    birthday: '1965-01-01'
  },
  billing: {
    name: 'João das Neves',
    address: {
      country: 'br',
      state: 'sp',
      city: 'Cotia',
      neighborhood: 'Rio Cotia',
      street: 'Rua Matrix',
      street_number: '9999',
      zipcode: '06714360'
    }
  },
  shipping: {
    name: 'Neo Reeves',
    fee: 1000,
    delivery_date: '2000-12-21',
    expedited: true,
    address: {
      country: 'br',
      state: 'sp',
      city: 'Cotia',
      neighborhood: 'Rio Cotia',
      street: 'Rua Matrix',
      street_number: '9999',
      zipcode: '06714360'
    }
  },
  items: []
};

const apiKey = 'ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd';

module.exports = { request, apiKey };
