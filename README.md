#Desafio por Max Araújo

Desafio para a Totvs de um aplicativo de e-commerce.

###Como Executar

- Entre no caminho raiz do repositório e execute:
``` docker-compose up ```

- Para executar precisa ter instalado o **docker** e **docker-compose** *

- A aplicação está executando em localhost:8080

####Guarde o token (nonce) da sessão do Carrinho para poder alterar as informações da mesma.

- Faça as requisições da documentação no Postman:

| Método     | Path                   |  Description                                                         |
| :---       |     :---:              |          ---:                                                        |
| GET        | /products              | Lista os produtos                                                    |
| POST       | /products              | Insere os produtos                                                   |
| GET        | /cart                  | Retorna o carrinho do usuário                                        |
| POST       | /cart                  | Inserir produtos no carrinho, utilizar o Nonce (Token do Carrinho)   |
| DELETE     | /cart/empty/:nonce     | Deleta todos os produtos do carrinho do especifico Nonce (Token)     |
| DELETE     | /cart/empty/:nonce/:id | Deleta um produto especifico pelo Id de um carrinho                  |
| POST       | /cart/update           | Atualiza a quantidade de um produto no carrinho                      |
| POST       | /checkout              | Realiza o pagamento com as informações em Json definidas             |

### Para cadastrar um produto
- POST  /products
Coloque no body esse .json para inserir um produto por exemplo.
```
{
  "name" : "Nome do Produto",
  "description" : "Descrição do Produto",
  "imageUrl" : "Caminho da imagem do produto",
  "value": "Valor do Produto",
  "factor": "Utilizar a letra 'A', 'B' ou 'C'
}
```

### Para verificar o Id dos produtos
- GET /products

### Para inserir o produto ao carrinho
- POST /cart
Insira esse Json no Body
```
{
        "_id": "//id do produto"
        "qty": "//quantidade de produtos",
        "nonce": "token (nonce) da sessão que é pega em localhost:8080"
}
```

### Para fazer o checkout do carrinho no pagarme
- POST /checkout

### Possui documentação Swagger no arquivo doc.yaml

### A versão atual não possui testes unitários, mas serão implementados futuramente
















