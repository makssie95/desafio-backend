swagger: '2.0'
x-api-id: 'desafio-totvs-carrinho-de-compras'

################################################################################
#                              API Information                                 #
################################################################################
info:
  version: "1.0.0"
  title: Desafio Totvs - Carrinho de Compras

host: localhost:8080

schemes:
  - http

produces:
  - application/json
  - text/csv

consumes:
  - application/json

tags:
- name: "Products"
  description: "Operations about products"
- name: "Cart"
  description: "Operations about cart"
- name: "Checkout"
  description: "Operations about checkout"


################################################################################
#                                Paths                                         #
################################################################################
paths:
  /products:
    get:
      tags:
        - "Products"
      description: Retorna uma lista de produtos disponíveis
      summary: Retorna uma lista de produtos disponíveis
      responses:
        200:
          description: Sucesso no retorno
          schema:
            $ref: '#/definitions/CategoryArray'
        500:
          description: Erro inesperado
          schema:
            $ref: '#/definitions/Error'
    post:
      tags:
        - "Products"
      description: Inclui um novo produto na lista de produtos.
      summary: Inserir um novo produto na lista
      consumes:
        - application/json
      parameters:
        - in: body
          name: product
          description : Produto a ser adicionado
          schema:
            $ref: '#/definitions/Products'
      responses:
        200:
          description: Sucesso no retorno
          schema:
            $ref: '#/definitions/Products'
        500:
          description: Erro inesperado
          schema:
            $ref: '#/definitions/Error'

  /cart:
    get:
      tags:
        - "Cart"
      description: Retorna o carrinho do usuário validado por Nounce (Token)
      summary: Retorna o carrinho do usuário validado por Nounce (Token)
      responses:
        200:
          description: Sucesso no retorno
          schema:
            $ref: '#/definitions/CartArray'
        500:
          description: Erro inesperado
          schema:
            $ref: '#/definitions/Error'
    post:
      tags:
        - "Cart"
      description: Inserir produtos no carrinho, utilizar o Nounce (Token do Carrinho)
      summary: Inserir produtos no carrinho
      consumes:
        - application/json
      parameters:
        - in: body
          name: product
          description : Produto a ser adicionado
          schema:
            $ref: '#/definitions/CartItem'
      responses:
        200:
          description: Sucesso ao inserir
          schema:
            $ref: '#/definitions/CartItem'

        500:
          description: Erro Inesperado
          schema:
            $ref: '#/definitions/Error'

  /cart/empty/:nonce:
    delete:
      tags:
        - "Cart"
      description: No parametro nonce utilizar o token (nonce) do carrinho
      summary: Deletar todos os produtos do carrinho
      responses:
        200:
          description: Sucesso ao deletar
        500:
          description: Erro inesperado

  /cart/empty/:nonce/:id:
    delete:
      tags:
        - "Cart"
      description: Nos parametros utilizar o nonce (token) do carrinho e o Id do produto
      summary: Deletar produto especifico do carrinho
      responses:
        200:
          description: Sucesso ao deletar um produto
        500:
          description: Erro inesperado

  /cart/update:
    post:
      tags:
        - "Cart"
      description: Atualizar quantidade de produto no carrinho pelo id
      summary: Atualizar quantidade de produto no carrinho pelo id
      responses:
        200:
          description: Sucesso ao atualizar a quantidade do produto
          schema:
            $ref: '#/definitions/CartItem'
        500:
          description: Erro inesperado
          schema:
            $ref: '#/definitions/Error'

  /checkout:
    post:
      tags:
        - "Checkout"
      description: Efetua pagamento ao Pagar.me
      summary: Efetua pagamento ao Pagar.me
      responses:
        200:
          description: Sucesso do pagamento
        500:
          description: Erro inesperado



################################################################################
#                                Definitions                                   #
################################################################################
definitions:

  Products:
    description: Produto
    type: object
    properties:
      name:
        description: Nome do produto
        type: string
      description:
        description: Descrição do produto
        type: string
      factor:
        description: Tipo do Fator do produto
        type: string
        enum: [A, B, C]
      value:
        description: Preço do Produto
        type: number
      imageUrl:
        description: Url da imagem
        type: string
  CategoryArray:
    type: object
    description: Meta objeto com dados de paginação e uma lista de categorias
    properties:
      objects:
        type: array
        items:
          $ref: '#/definitions/Products'
  CartArray:
    description: Carrinho de Produtos
    type: object
    properties:
      pageTitle:
        description: Nome do Titulo da Página
      cart:
        type: object
        properties:
          items:
            type: array
            items:
              $ref: '#/definitions/Products'
          totals:
            description: valor do total do carrinho com desconto
            type: number
          discount:
            description: desconto aplicado para o valor total
            type: number
      nounce:
        type: string
        description: token de acesso
  CartItem:
    description: item para inserir
    type: object
    properties:
      _id:
        type: string
        description: Id do produto para inserir
      qty:
        type: number
        description: Quantidade de itens do produto para inserir
      nonce:
        type: string
        description: Token do carrinho para identificação
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
